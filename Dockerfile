FROM postgres:latest
COPY ./db_scripts/setup.sh /docker-entrypoint-initdb.d/
ENV PYTHONUNBUFFERED 1