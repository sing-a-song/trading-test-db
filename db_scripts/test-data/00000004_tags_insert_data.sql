INSERT INTO "tags" ("tags_id", "content") VALUES
(1,'Healthcare'),
(2,'Long Ideas'),
(3,'REITs'),
(4,'Forex'),
(5,'Consumer');

INSERT INTO "articles_tags" ("articles_id", "tags_id") VALUES
(19846,1),
(19847,2),
(19847,3),
(19847,5),
(19851,4),
(19852,1),
(19852,2),
(19852,3),
(19852,4);
