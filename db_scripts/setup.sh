#!/usr/bin/env bash

# Terminate all existing connections
psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity
                     WHERE pg_stat_activity.datname = 'trading' AND pid <> pg_backend_pid();"

# create DB if not exists
psql -U postgres -c "DROP DATABASE IF EXISTS trading;"
psql -U postgres -c "CREATE DATABASE trading;"

# # Set timezone
psql -U postgres trading -c "SET TIME ZONE 'Europe/Prague';"

for FILE in $(ls /tmp/db_scripts/schema)
do
	psql -U postgres trading -f "/tmp/db_scripts/schema/$FILE"
done;

for FILE in $(ls /tmp/db_scripts/test-data)
do
	psql -U postgres trading -f "/tmp/db_scripts/test-data/$FILE"
done;