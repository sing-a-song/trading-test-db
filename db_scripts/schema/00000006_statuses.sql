DROP TABLE IF EXISTS statuses;

DROP SEQUENCE IF EXISTS statuses_id_seq;
CREATE SEQUENCE statuses_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE statuses(
  statuses_id integer DEFAULT nextval('statuses_id_seq') NOT NULL,
  code varchar(10),
  description varchar(100),
  CONSTRAINT statusesPK PRIMARY KEY (statuses_id)
);

INSERT INTO statuses (statuses_id, code, description) values
(1, '0','OK'),
(2, '1', 'Too short to summarize.'),
(3, '2','Unable to summarize. Probably non-textual content.');

ALTER TABLE articles_summaries
ADD COLUMN statuses_id integer,
ADD CONSTRAINT articles_summariesFK3 FOREIGN KEY (statuses_id) REFERENCES statuses(statuses_id);
