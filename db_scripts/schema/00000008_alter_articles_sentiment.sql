CREATE TABLE sentiments(
  sentiments_id integer NOT NULL,
  name varchar(10) NOT NULL,
  CONSTRAINT sentimentsPK PRIMARY KEY (sentiments_id)
);
INSERT INTO  sentiments VALUES(1,'positive');
INSERT INTO  sentiments VALUES(2,'negative');
INSERT INTO  sentiments VALUES(3,'neutral');


ALTER TABLE articles_sentiments RENAME COLUMN article_sentiment TO sentiments_id;
ALTER TABLE articles_sentiments ADD CONSTRAINT articles_sentimentsFK4 FOREIGN KEY (sentiments_id) REFERENCES sentiments(sentiments_id);
ALTER TABLE articles_sentiments ADD COLUMN article_sentiment_value real NOT NULL;
