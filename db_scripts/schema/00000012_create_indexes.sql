CREATE INDEX "articles_tickers_articles_id" ON "articles_tickers" ("articles_id");

CREATE INDEX "articles_sentiments_articles_id" ON "articles_sentiments" ("articles_id");

CREATE INDEX "articles_summaries_articles_id" ON "articles_summaries" ("articles_id");