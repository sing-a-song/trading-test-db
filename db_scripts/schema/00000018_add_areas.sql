CREATE TABLE areas(
  areas_id serial NOT NULL,
  areas_name text NOT NULL,
  CONSTRAINT areas_PK PRIMARY KEY (areas_id),
  CONSTRAINT areas_name_UK UNIQUE (areas_name)
);

CREATE TABLE articles_areas(
  articles_id integer NOT NULL,
  areas_id integer NOT NULL,
  CONSTRAINT articles_areas_PK PRIMARY KEY (articles_id,areas_id),
  CONSTRAINT articles_areas_areas_id_FK FOREIGN KEY (areas_id) REFERENCES areas(areas_id)
);
