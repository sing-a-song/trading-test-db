DROP TABLE IF EXISTS articles_sentiments_tickers;
DROP SEQUENCE IF EXISTS articles_sentiments_tickers_id_seq;

CREATE SEQUENCE articles_sentiments_tickers_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;


CREATE TABLE articles_sentiments_tickers(
  articles_sentiments_tickers_id integer DEFAULT nextval('articles_sentiments_tickers_id_seq') NOT NULL,
  articles_id integer NOT NULL,
  tickers_id integer NOT NULL,
  sentiments_id integer,
  description text,
  tools_id integer NOT NULL,
  sentiment_time timestamp with time zone NOT NULL,
  CONSTRAINT articles_sentiments_tickersPK PRIMARY KEY (articles_sentiments_tickers_id),
  CONSTRAINT articles_sentiments_tickersFK2 FOREIGN KEY (tools_id) REFERENCES tools(tools_id),
  CONSTRAINT articles_sentiments_tickersFK3 FOREIGN KEY (tickers_id) REFERENCES tickers(tickers_id),
  CONSTRAINT articles_sentiments_tickersFK4 FOREIGN KEY (sentiments_id) REFERENCES sentiments(sentiments_id)

);

ALTER TABLE "articles_sentiments_tickers"
ADD CONSTRAINT "articles_sentiments_tickers_articles_id_tickers_id_article_sentiment_tools_id" UNIQUE ("articles_id", "tickers_id", "sentiments_id", "tools_id");