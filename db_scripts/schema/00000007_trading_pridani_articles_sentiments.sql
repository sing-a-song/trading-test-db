DROP TABLE IF EXISTS articles_sentiments;
DROP SEQUENCE IF EXISTS articles_sentiments_id_seq;

CREATE SEQUENCE articles_sentiments_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE articles_sentiments(
  articles_sentiments_id integer DEFAULT nextval('articles_sentiments_id_seq') NOT NULL,
  article_sentiment integer,
  articles_id integer NOT NULL,
  tools_id integer NOT NULL,
  params text,
  sentiment_time timestamp with time zone NOT NULL,
  statuses_id integer,
  CONSTRAINT articles_sentimentsPK PRIMARY KEY (articles_sentiments_id),
  CONSTRAINT articles_sentimentsFK1 FOREIGN KEY (articles_id) REFERENCES articles(articles_id),
  CONSTRAINT articles_sentimentsFK2 FOREIGN KEY (tools_id) REFERENCES tools(tools_id),
  CONSTRAINT articles_sentimentsFK3 FOREIGN KEY (statuses_id) REFERENCES statuses(statuses_id)
);


