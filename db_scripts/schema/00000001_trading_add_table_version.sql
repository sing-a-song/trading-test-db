CREATE TABLE versions(
  version_id integer NOT NULL,
  date_time timestamp NOT NULL,
  CONSTRAINT version_id PRIMARY KEY (version_id)
);

INSERT INTO versions(version_id,date_time) VALUES (1, now());