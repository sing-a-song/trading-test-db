CREATE TABLE industries(
industries_id serial NOT NULL PRIMARY KEY,
name text NOT NULL,
sectors_id integer);

ALTER TABLE industries ADD CONSTRAINT industries_sectors_id_FK FOREIGN KEY (sectors_id) REFERENCES sectors(sectors_id);

ALTER TABLE tickers ADD industries_id integer;

ALTER TABLE tickers ADD CONSTRAINT tickers_industries_id_FK FOREIGN KEY (industries_id) REFERENCES industries(industries_id)
