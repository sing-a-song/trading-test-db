CREATE TABLE "tickers_trend_cache" (
  "rank" integer NOT NULL,
  "start_date" date NOT NULL,
  "end_date" date NOT NULL,
  "tickers_id" integer NOT NULL,
  "articles_count_in_period" integer NOT NULL,
  "symbol" text NOT NULL,
  "title" text NULL,
  "articles_mean_count_in_period" real NOT NULL,
  "standard_deviation" real NOT NULL,
  "deviation_rate" real NOT NULL
);