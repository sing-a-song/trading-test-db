CREATE TABLE stock_indexes(
  stock_indexes_id serial NOT NULL,
  stock_indexes_abbreviation text NOT NULL,
  CONSTRAINT stock_indexes_PK PRIMARY KEY (stock_indexes_id),
  CONSTRAINT stock_indexes_abbreviation_UK UNIQUE (stock_indexes_abbreviation)
);

CREATE TABLE stock_index_relations(
  stock_indexes_id1 integer NOT NULL,
  stock_indexes_id2 integer NOT NULL,
  CONSTRAINT stock_index_relations_PK PRIMARY KEY (stock_indexes_id1,stock_indexes_id2),
  CONSTRAINT stock_index_relations_stock_indexes_id1_FK FOREIGN KEY (stock_indexes_id1) REFERENCES stock_indexes(stock_indexes_id),
  CONSTRAINT stock_index_relations_stock_indexes_id2_FK FOREIGN KEY (stock_indexes_id2) REFERENCES stock_indexes(stock_indexes_id)
);

ALTER TABLE tickers
ADD COLUMN stock_indexes_id integer;

ALTER TABLE tickers
ADD CONSTRAINT tickers_stock_indexes_id FOREIGN KEY (stock_indexes_id) REFERENCES stock_indexes(stock_indexes_id);
