DROP TABLE IF EXISTS articles_summaries;
DROP SEQUENCE IF EXISTS articles_summaries_id_seq;

DROP TABLE IF EXISTS articles;
DROP SEQUENCE IF EXISTS articles_id_seq;

DROP TABLE IF EXISTS articles_categories;
DROP SEQUENCE IF EXISTS articles_categories_id_seq;

CREATE SEQUENCE articles_categories_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE articles_categories(
  articles_categories_id integer DEFAULT nextval('articles_categories_id_seq') NOT NULL,
  article_category text UNIQUE NOT NULL,
  CONSTRAINT articles_categoriesPK PRIMARY KEY (articles_categories_id)
);

CREATE SEQUENCE articles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE articles(
  articles_id integer DEFAULT nextval('articles_id_seq') NOT NULL,
  article_title text NOT NULL,
  retrieved_at timestamp with time zone NOT NULL,
  published_at timestamp with time zone NOT NULL,
  lang character varying(10),
  domain text NOT NULL,
  url text UNIQUE NOT NULL,
  articles_categories_id int NOT NULL,
  author_summary text[],
  article_text text,
  CONSTRAINT articlesPK PRIMARY KEY (articles_id),
  CONSTRAINT articles_categoriesFK FOREIGN KEY (articles_categories_id) REFERENCES articles_categories(articles_categories_id)
);

DROP TABLE IF EXISTS tools;
DROP SEQUENCE IF EXISTS tools_id_seq;
CREATE SEQUENCE tools_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE tools(
  tools_id integer DEFAULT nextval('tools_id_seq') NOT NULL,
  name text UNIQUE NOT NULL,
  CONSTRAINT toolsPK PRIMARY KEY (tools_id)
);

INSERT INTO "tools" ("tools_id", "name") VALUES
(1, 'gensim'),
(2, 'extractive - facebook/bart-large-cnn; abstractive - google/pegasus-cnn_dailymail'),
(3, 'Bert sentiment');

CREATE SEQUENCE articles_summaries_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE articles_summaries(
  articles_summaries_id integer DEFAULT nextval('articles_summaries_id_seq') NOT NULL,
  article_summary text,
  articles_id integer NOT NULL,
  tools_id integer NOT NULL,
  params text,
  CONSTRAINT articles_summariesPK PRIMARY KEY (articles_summaries_id),
  CONSTRAINT articles_summariesFK1 FOREIGN KEY (articles_id) REFERENCES articles(articles_id),
  CONSTRAINT articles_summariesFK2 FOREIGN KEY (tools_id) REFERENCES tools(tools_id)
);

DROP TABLE IF EXISTS articles_tickers;
DROP TYPE IF EXISTS ticker_role;

DROP TABLE IF EXISTS tickers;
DROP SEQUENCE IF EXISTS tickers_id_seq;
CREATE SEQUENCE tickers_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE tickers(
  tickers_id integer DEFAULT nextval('tickers_id_seq') NOT NULL,
  symbol text UNIQUE NOT NULL,
  title text,
  CONSTRAINT tickersPK PRIMARY KEY (tickers_id)
);

CREATE TYPE ticker_role AS ENUM ('about', 'includes');
CREATE TABLE articles_tickers(
  articles_id integer NOT NULL,
  tickers_id integer NOT NULL,
  role ticker_role,
  CONSTRAINT articles_tickersPK PRIMARY KEY (articles_id, tickers_id, role),
  CONSTRAINT articlesFK1 FOREIGN KEY (articles_id) REFERENCES articles(articles_id) ON DELETE CASCADE,
  CONSTRAINT tickersFK1 FOREIGN KEY (tickers_id) REFERENCES tickers(tickers_id)
);