DROP TABLE IF EXISTS "sectors";
DROP SEQUENCE IF EXISTS sectors_id_seq;
CREATE SEQUENCE sectors_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."sectors" (
    "sectors_id" integer DEFAULT nextval('sectors_id_seq') NOT NULL,
    "name" text NOT NULL,
    CONSTRAINT "sectors_name_key" UNIQUE ("name"),
    CONSTRAINT "sectorspk" PRIMARY KEY ("sectors_id")
) WITH (oids = false);

INSERT INTO "sectors" ("sectors_id", "name") VALUES
(1,	'Financial Services'),
(2,	'Healthcare'),
(3,	'Technology'),
(4,	'Consumer Cyclical'),
(5,	'Consumer Defensive'),
(6,	'Utilities'),
(7,	'Real Estate'),
(8,	'Basic Materials'),
(9,	'Industrials'),
(10,	'Communication Services'),
(11,	'Energy'),
(12,	'Industrial Goods'),
(13,	'Financial'),
(14,	'Services');