DROP TABLE IF EXISTS articles_tags;
DROP TABLE IF EXISTS tags;

DROP SEQUENCE IF EXISTS tags_id_seq;
CREATE SEQUENCE tags_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE tags(
  tags_id integer DEFAULT nextval('tags_id_seq') NOT NULL,
  content text UNIQUE NOT NULL,
  CONSTRAINT tagsPK PRIMARY KEY (tags_id)
);

CREATE TABLE articles_tags(
  articles_id integer NOT NULL,
  tags_id integer NOT NULL,
  CONSTRAINT articles_tagsPK PRIMARY KEY (articles_id, tags_id),
  CONSTRAINT articlesFK1 FOREIGN KEY (articles_id) REFERENCES articles(articles_id) ON DELETE CASCADE,
  CONSTRAINT tagsFK1 FOREIGN KEY (tags_id) REFERENCES tags(tags_id)
);

